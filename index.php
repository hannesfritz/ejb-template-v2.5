<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
<meta name="google-site-verification" content="QdQZIrl1mswDmHP3Y6sR7Yb_vyog9NGkrNZio_TinYg" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/css/template.css" type="text/css"/>
<link rel="alternate" href="http://feeds.feedburner.com/ejwbesigheim" type="application/rss+xml" />
<link rel="apple-touch-icon-precomposed" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/touch-icon-iphone.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/touch-icon-ipad.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/touch-icon-iphone-retina.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/touch-icon-ipad-retina.png" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<jdoc:include type="head" />
</head>

<body>

<!--
<div id="userheader">
  <div id="loginbox">
    <jdoc:include type="modules" name="login" style="xhtml" />
  </div>

  <div id="suche">
    <jdoc:include type="modules" name="suche" style="xhtml" />
  </div>
</div>
-->

<!-- Nur zum Menutest -->
<div id="topNav">
    <jdoc:include type="modules" name="top" style="xhtml" />
</div>
<!-- End of Test -->

<div id="header">
  <a href="http://ejwbesigheim.de"><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/ejb-banner.jpg" width="900px" height="150px" alt="Headerbild" /></a>
</div>

<a name="top"></a>
  
<div id="inhalt">
  <div class="menu">
    <jdoc:include type="modules" name="left" style="xhtml" />
  </div>
    <div class="blockrechts">
    <jdoc:include type="modules" name="blockrechts" style="xhtml" />
  </div>
  <div class="content">
    <jdoc:include type="component" />
  </div>
</div>  

  <div id="footer">
    <p id="nachoben"><a href="#top">nach oben &#x25B2;</a></p>
    <br />
      <div class="spalte">
        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/zeiten.png" alt="Uhr-Icon" />
        <h5>treffen</h5>
        <p>&Ouml;ffnungszeiten des ejb-B&uuml;ros:</p>
        <table>
          <tr>
            <th></th>
            <th></th>
          </tr>
          <tr class="rot">
            <td class="borderrechts">Mo</td>
            <td>geschlossen</td>
          </tr>
          <tr class="gruen">
            <td class="borderrechts">Di - Do</td>
            <td>9-12 Uhr, 15-18 Uhr</td>
          </tr>
          <tr class="orange">
            <td class="borderrechts">Fr</td>
            <td>nach Vereinbarung</td>
          </tr>
        </table>
        <p><a href="http://www.ejwbesigheim.de/wir-ueber-uns/bueroteam">&#x25BA; zum B&uuml;roteam</a></p>  
      </div>
      <div class="spalte">
        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/registrieren.png" alt="Tuer-Icon" />
        <h5>registrieren</h5>
        <p><em>Die Registrierung ist zur Zeit noch deaktiviert.</em></p>
      </div>
      <div class="spalte">
        <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/informieren.png" alt="Info-Icon"/>
        <h5>informieren</h5>
        <p>Bleibe auf dem Laufenden, indem du unseren <a href="http://feeds.feedburner.com/ejwbesigheim?format=xml">Newsfeed abonnierst</a></p>
      <!--   <p>dich bei unseren Newslettern anmeldest</p> -->
        <p>oder uns auf Social Networks folgst:</p>
        <p><a href="http://twitter.com/ejwbesigheim">&#x25BA; ejb auf Twitter</a></p>
        <p><a href="http://www.facebook.com/ejw.besigheim">&#x25BA; ejb auf Facebook</a></p>
      </div>
      <div class="spalte">
      <img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/media/kontaktieren.png" alt="Kontakt-Icon" />
        <h5>kontaktieren</h5>
        <p>Gib uns Feedback, schicke Terminvorschl&auml;ge oder stell uns deine Fragen.</p>
        <p><a href="mailto:webmaster@ejwbesigheim.de">&#x25BA; Mail an das Webteam</a></p>
        <p><a href="http://www.ejwbesigheim.de/wir-ueber-uns/webteam">&#x25BA; zum Webteam</a></p>    
      </div>
  </div> 
  <p id="subfooter">&copy; Evang. Jugendwerk Bezirk Besigheim | <a href="http://ejwbesigheim.de/index.php?option=com_xmap&view=xml&tmpl=component&id=1" title="Sitemap des ejb">Sitemap</a> | <a href="http://www.ejwbesigheim.de/service/archiv" title="Artikelarchiv">Archiv</a> | <a href="http://www.ejwbesigheim.de/impressum" title="ejb Impressum">Impressum</a></p>

<!-- Mootols -->
<?php JHtml::_('behavior.framework', true); ?>

</body>
</html>
